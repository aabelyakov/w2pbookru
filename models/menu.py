#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#############################################################################
#                                w2pbookru
#############################################################################
__revision__ = "$Id$"
#############################################################################
#response.title = request.application
response.title = "W2pBookRu"
response.subtitle = 'Книга о web-фреймворке web2py на русском языке!'
response.keywords = 'web framework, python, web2py'
response.description = 'Книга о web-фреймворке web2py (2-е издание) на русском языке ' 
response.author = "А.А.Беляков"
#############################################################################