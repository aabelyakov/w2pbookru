#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#############################################################################
#                                w2pbookru
#############################################################################
__revision__ = "$Id$"
#============================================================================
def plugin_wmd():
    script = SCRIPT('wmd_options = {output: "Markdown", autostart: true};')
    src = URL(r=request, c='static', f='plugin_wmd/wmd.js')
    return TAG[''](DIV(_class="wmd-preview"), script, SCRIPT(_src=src))
#enddef
#############################################################################
