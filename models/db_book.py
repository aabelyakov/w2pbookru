#!/usr/bin/env python
# -*- coding: utf-8 -*-  
#############################################################################
#                                w2pbookru
#############################################################################
import re
import os
import sys
import difflib
from gluon.contrib.markdown import WIKI 
#############################################################################
__revision__ = "$Id$"
#############################################################################
RECORD_HISTORY = True
ALWAYS_RELOAD_DOCSTRING = False
#----------------------------------------------------------------------------
# Определения часто используемых полей для таблиц
#----------------------------------------------------------------------------
# Определение поля "Автор создания или изменения элемента (главы или 
# параграфа) книги"
created_by = Field(
    'created_by',
    db.auth_user,
    #default = auth.user_id,
    #update = auth.user_id,
    readable = True,
    writable = False
)
#----------------------------------------------------------------------------
# Определение поля "Время создания или изменения элемента (главы или 
# параграфа) книги"
created_on = Field(
    'created_on',
    'datetime',
    default = request.now,
    update = request.now,
    readable = True,
    writable = False
)
#----------------------------------------------------------------------------
# Определение поля "Показывать элемент книги"
active = Field('active', 'boolean', default = True)
#----------------------------------------------------------------------------
# Определение поля "Различия"
diff = Field('diff','text',default = '',readable = False,writable = False)
#----------------------------------------------------------------------------
# Журнал изменений
changelog = Field('changelog')
#============================================================================
# Определение таблицы section - элемент (глава или раздел) книги
db.define_table(
    'section',
    #------------------------------------------------------------------------
    # Номер главы 
    Field('chapter_number', 'integer'),
    #------------------------------------------------------------------------
    # Номер раздела
    Field('section_number', 'integer'),
    #------------------------------------------------------------------------
    # Заголовок главы или раздела
    Field('title'),
    #------------------------------------------------------------------------
    # Содержание главы или раздела 
    Field('content','text'),
    #------------------------------------------------------------------------
    # Различия между ревизиями
    diff,
    #------------------------------------------------------------------------
    # Автор создания или изменения элемента книги
    created_by,
    #------------------------------------------------------------------------
    # Время создания или изменения элемента книги
    created_on,
    #------------------------------------------------------------------------
    # Показывать - True, Не показывать - False
    active,
    #------------------------------------------------------------------------
    # Журнал изменений
    changelog,
    #------------------------------------------------------------------------
    # Колонка таблицы для отображения в выпадающем списке
    format = '%(title)s'
)
#============================================================================
# Определение таблицы wikipage - Вики-страница
db.define_table(
    'wikipage',
    Field('section',db.section),
    Field('title', requires=(
        IS_NOT_EMPTY(),
        IS_NOT_IN_DB(db,'wikipage.title')
    )),
    Field('slug', readable=False, writable=False),
    Field('content','text'),
    diff,
    created_by,
    created_on,
    active,
    changelog,
    format = '%(title)s'
)
#============================================================================
# Определение таблицы docstring - Строка документирования
db.define_table(
    'docstring',
    Field('title', requires=IS_MATCH('^[\w\.-]$')),
    Field('content','text'),
    diff,
    created_by,
    created_on,
    active,
    changelog,
    format = '%(title)s'
)
#============================================================================
# Определение таблицы document
db.define_table(
    'document',
    Field('title', requires=(
        IS_NOT_EMPTY(),
        IS_NOT_IN_DB(db,'document.title')
    )),
    Field('file','upload', requires=IS_NOT_EMPTY()),
    created_by,
    created_on,
    format = '%(title)s'
)
#============================================================================
# Определение таблиц section_history, wikipage_history, docstring_history
if RECORD_HISTORY:
    for tablename in ['section','wikipage','docstring']:
        db.define_table(
            tablename + '_history',
            Field(tablename, db[tablename]),
            db[tablename]
        )
    #endfor
#endif    
#============================================================================
def clone(table, old_record):
    cache.ram.clear()
    #------------------------------------------------------------------------
    if RECORD_HISTORY and old_record:
        row = {}
        #--------------------------------------------------------------------
        for field in table.fields:
            if field in old_record:
                row[field if field != ' id' else table._tablename] = \
                    old_record[field]
            #endif
        #endfor
        #--------------------------------------------------------------------
        db['%s_history' % table].insert(**row)
    #endif    
#enddef
#============================================================================
def differ(table, form):
    new_record = form.vars
    old_record = form.record
    #------------------------------------------------------------------------
    if old_record:
        d1 = list(difflib.Differ().compare(
            old_record.content.split('\n'),
            new_record.content.split('\n')
        ))
        #--------------------------------------------------------------------
        new_record.diff = '\n'.join([x for x in d1 if x[:1] in ['+', '-']])
    #endif
#enddef
#============================================================================
if len(db(db.auth_user.id > 0).select()) == 0:
    # Таблица auth_user пуста
    #------------------------------------------------------------------------
    db.auth_user.insert(
        first_name = 'Massimo',
        last_name = 'Di Pierro',
        nickname = 'mdp',
        email = 'mdipierro@cs.depaul.edu',
        password = db.auth_user.password.requires[0]('test')[0],
        registration_key = ''
    )
    #------------------------------------------------------------------------
    db.auth_user.insert(
        first_name = 'Anatoly',
        last_name = 'Belyakov',
        nickname = 'aabelyakov',
        email = 'aabelyakov@mail.ru',
        password = db.auth_user.password.requires[0]('w2pbookru')[0],
        registration_key = ''
    )
#endif    
#----------------------------------------------------------------------------
# Запись данных в таблицу section, если она пуста
if len(db(db.section.id > 0).select()) == 0: 
    # Таблица section пуста
    #------------------------------------------------------------------------
    for i in range(0, 13):
        # Цикл по главам книги 
        #--------------------------------------------------------------------
        # Чтение содержимого текущей главы из файла chapXX.tek, 
        # расположенного в каталоге /w2pbookru/private
        data = open(
            os.path.join(
                request.folder,
                'private',
                'chap%02d.tek' % i
                ), 'r'
            ).read()
        #--------------------------------------------------------------------
        # Содержимое файла преобразовано с помощью разделителя \n в список 
        # из двух строк: заголовок главы и content.
        # Нулевой элемент списка - это заголовок главы.
        # Подстрока [1:] - это заголовок главы без ведущего #.
        # strip() - убирает из строки ведущие и концевые пробелы
        #title = data.split('\n', 1)[0][1:].strip()
        #--------------------------------------------------------------------
        for k, cnt in enumerate(data.split('\n## ')):      
            # Нумерованный цикл по разделам книги   
            # После разбиения сама глава находится в первом разделе
            #----------------------------------------------------------------
            if k == 0:
                # Это глава
                #------------------------------------------------------------
                # К содержимому главы впереди добавляем номер главы
                content = "%d. " % i + cnt[2:]
                #------------------------------------------------------------
                # Содержимое главы преобразовано с помощью разделителя \n в
                # список из двух строк: заголовка главы - title и 
                # содержимого главы - content.
                # Нулевой элемент списка - это заголовок главы - title.
                title = content.split('\n', 1)[0].replace("**", "")
            else:
                # Это раздел
                #------------------------------------------------------------
                # К содержимому раздела впереди добавляем номер раздела
                content = '%d.%d. ' % (i, k) + cnt
                #------------------------------------------------------------
                # Содержимое раздела преобразовано с помощью разделителя \n в
                # список из двух строк: заголовка раздела - title и 
                # содержимого раздела - content.
                # Нулевой элемент списка - это заголовок главы - title.
                # strip() - убирает из строки ведущие и концевые пробелы
                title = content.split('\n', 1)[0].replace("**", "")
            #endif
            #----------------------------------------------------------------
            # Вставка содержимого главы или раздела в таблицу section БД.
            # При вставке в таблицу section новых строк, должно заполняться
            # поле created_by (auth_user.id), чтобы было ясно каким именно
            # пользователем производится заполнение текущей строки таблицы. 
            # Пусть этим пользователем буду я. В таблице auth_user для меня  
            # значение поля id (Primary Key) равно 2, тогда как у MDP - 1! 
            # Следовательно, в таблице section содержимое поле created_by
            # (Foreign Key) будет равно 2.
            db.section.insert(
                title = title, 
                content = content,
                chapter_number = i,
                section_number = k,
                created_by = 2
            )
        #endfor
    #endfor   
#endif 
#----------------------------------------------------------------------------
# Сбор оглавления книги
toc = db(db.section.active == True).select(
    db.section.chapter_number,
    db.section.section_number,
    db.section.title,
    orderby = db.section.chapter_number | db.section.section_number,
    cache = (cache.ram, 3600)
)
#============================================================================
def wiki(text):
    ### fix internal references
    text = re.sub(
        '\]\(book_image/(?P<name>.*?)\)',
        '](/%s/static/book_image/\g<name>)' % request.application, text
    )
    #------------------------------------------------------------------------
    text = re.sub(
        '\]\(document/(?P<name>.*?)\)',
        '](/%s/default/download/\g<name>)' % request.application, text
    )
    #------------------------------------------------------------------------
    text = re.sub(
        '\]\(section/(?P<name>.*?)\)',
        '](/%s/default/section/\g<name>)' % request.application, text
    )
    #------------------------------------------------------------------------
    text = re.sub(
        '\]\(docstring/(?P<name>.*?)\)',
        '](/%s/default/docstring/\g<name>)' % request.application, text
    )
    #------------------------------------------------------------------------
    text = re.sub(
        '\]\(wikipage/(?P<name>.*?)\)',
        '](/%s/default/wikipage/\g<name>)' % request.application, text
    )
    #------------------------------------------------------------------------
    ### ability to interpret some reST syntax
    text = re.compile(
        '^[ ]*:(?P<key>.+?):(?![\w-])',
        re.M
        ).sub('* **\g<key>:**',text)
    #------------------------------------------------------------------------
    text = re.sub('::(?P<key>\s+)',':\g<key>',text)
    #------------------------------------------------------------------------
    t = WIKI(text).xml()
    #------------------------------------------------------------------------
    # syntax highlight the code
    r = re.compile('<pre>\s*<code>(?P<code>.*?)</code>\s*</pre>', re.S)
    #------------------------------------------------------------------------
    while True:
        # Бесконечный цикл
        #--------------------------------------------------------------------
        m = r.search(t)
        #--------------------------------------------------------------------
        if not m:
            break
        #endif
        #--------------------------------------------------------------------
        link = URL(r=request, f='docstring')
        #--------------------------------------------------------------------
        t = t[:m.start()] + CODE(
            m.group('code').replace('&lt;', '<').replace('&gt;', '>').strip(),
            language = 'web2py',
            link = link
            ).xml() + t[m.end():]
    #endwhile    
    #------------------------------------------------------------------------
    return XML(t)
#enddef
#============================================================================
def tt(d):
    try:
        dt = request.now - d
    except:
        return ''
    #endtry
    #------------------------------------------------------------------------
    if dt.days >= 365 * 2:
        return '%s years ago' % int(dt.days/365)
    elif dt.days >= 365:
        return '1 years ago'
    elif dt.days >= 60:
        return '%s months ago' % int(dt.days/30)
    elif dt.days > 21:
        return '1 months ago'
    elif dt.days >= 14:
        return '%s weeks ago' % int(dt.days/7)
    elif dt.days >= 7:
        return '1 week ago'
    elif dt.days >= 2:
        return '%s days ago' % int(dt.days)
    elif dt.days == 1:
        return '1 day ago'
    else:
        return 'today'
    #endif
#enfdef
#============================================================================
def logformat(record):
    try:
        return "created by %s %s" % (
            record.created_by.nickname, 
            tt(record.created_on)
        )
    except:
        return "created by %s on %s" % (
            record.created_by, 
            record.created_on
        )
    #endtry
#enddef
#############################################################################
'''
                      sql.log
                      =======
timestamp: 2012-01-25T15:22:52.933976
CREATE TABLE auth_user(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    first_name CHAR(512),
    last_name CHAR(512),
    nickname CHAR(32),
    email CHAR(512),
    password CHAR(512),
    editor CHAR(1),
    last_login CHAR(512),
    registration_key CHAR(512),
    reset_password_key CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:53.043443
CREATE TABLE auth_group(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    role CHAR(512),
    description TEXT
);
success!
timestamp: 2012-01-25T15:22:53.110464
CREATE TABLE auth_membership(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    group_id INTEGER REFERENCES auth_group(id) ON DELETE CASCADE
);
success!
timestamp: 2012-01-25T15:22:53.282261
CREATE TABLE auth_permission(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    group_id INTEGER REFERENCES auth_group(id) ON DELETE CASCADE,
    name CHAR(512),
    table_name CHAR(512),
    record_id INTEGER
);
success!
timestamp: 2012-01-25T15:22:53.358512
CREATE TABLE auth_event(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    time_stamp TIMESTAMP,
    client_ip CHAR(512),
    user_id INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    origin CHAR(512),
    description TEXT
);
success!
timestamp: 2012-01-25T15:22:53.444170
CREATE TABLE auth_cas(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    service CHAR(512),
    ticket CHAR(512),
    renew CHAR(1)
);
success!
timestamp: 2012-01-25T15:22:54.020118
CREATE TABLE section(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    chapter_number INTEGER,
    section_number INTEGER,
    title CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.117133
CREATE TABLE wikipage(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    section INTEGER REFERENCES section(id) ON DELETE CASCADE,
    title CHAR(512),
    slug CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.263934
CREATE TABLE docstring(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.341695
CREATE TABLE document(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title CHAR(512),
    file CHAR(512),
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP
);
success!
timestamp: 2012-01-25T15:22:54.488482
CREATE TABLE section_history(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    section INTEGER REFERENCES section(id) ON DELETE CASCADE,
    chapter_number INTEGER,
    section_number INTEGER,
    title CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.609604
CREATE TABLE wikipage_history(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    wikipage INTEGER REFERENCES wikipage(id) ON DELETE CASCADE,
    section INTEGER REFERENCES section(id) ON DELETE CASCADE,
    title CHAR(512),
    slug CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.723189
CREATE TABLE docstring_history(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    docstring INTEGER REFERENCES docstring(id) ON DELETE CASCADE,
    title CHAR(512),
    content TEXT,
    diff TEXT,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP,
    active CHAR(1),
    changelog CHAR(512)
);
success!
timestamp: 2012-01-25T15:22:54.931427
CREATE TABLE plugin_comments_comment(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    tablename CHAR(512),
    record_id INTEGER,
    parent_node INTEGER,
    body CHAR(512),
    deleted CHAR(1),
    votes INTEGER,
    created_by INTEGER REFERENCES auth_user(id) ON DELETE CASCADE,
    created_on TIMESTAMP
);
success!
'''