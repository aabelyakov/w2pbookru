copy:
	rm errors/* | echo 'no errors'
	rm sessions/* | echo 'no session'
	rm *~ */*~ */*/*~ | echo 'no tilde files'
	tar zcvf ../web2py.app.book.w2p *
	scp -i ~/web2py.pem ../web2py.app.book.w2p ubuntu@web2py.com:~/
