#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Anatoly Belyakov  <aabelyakov@mail.ru>
'''
Здесь определены функции контроллера:
- функция index() - это функция, выполняемая "по умолчанию", если при вызове 
  ИФК не указана 
- функция user() - это функция, вызываемая для аутентификации (установления
  личности пользователя) и определения его прав доступа
- функция download() - это функция, вызываемая для выгрузки с сервера файла,
  ранее загруженного на него (в потоке) операцией upload
- функция call() - это функция, которая публикует в Интернете все сервисы,
  зарегистрированные на сервере (исключая те, которые используются "по 
  умолчанию") 
'''
#############################################################################
#                                w2pbookru
#############################################################################
def index():
    """
    ИФК, выполняемая "по умолчанию"
    """
    return dict()
#enddef
#============================================================================
def error(action='index', flash='Документ не найден!'):
    session.flash = flash
    redirect(url(action))
#enddef
#============================================================================
def search():
    """
    Полнотекстовый поиск по полю content таблицы section
    """
    if not request.vars.search or len(request.vars.search) < 3:
        error('index', "Поисковый запрос очень короткий!")
    #endif
    #------------------------------------------------------------------------
    '''
    sections = db(
        db.section.content.like('%' + request.vars.search + '%')).select(
            db.section.title,
            db.section.chapter_number,
            db.section.section_number
        )
    '''    
    sections = db(db.section.content.contains(request.vars.search)).select( 
        db.section.title,
        db.section.chapter_number,
        db.section.section_number
    )
    #------------------------------------------------------------------------
    if not sections: 
        error('index', 'Поиск не дал результатов!')
    #endif    
    #------------------------------------------------------------------------
    return dict(sections=sections)
#enddef
#============================================================================
#@cache(request.env.path_info, 3600)
def section():
    """
    Обработка элемента (главы или раздела) книги 
    Новый (изменённый) элемент книги занимает одну строку в таблице section
    Старый элемент книги занимает одну строку в таблице section_history
    Перед записью нового (изменённого) элемента книги в таблицу section 
    старый элемент записывается в историческую таблицу section_history
    
    При активации ссылки в меню книги эта функция вызывается с двумя
    аргументами chapter_number и section_number, которые находятся в списке
    request.args, например ['5', '0']    
    """
    #------------------------------------------------------------------------
    if request.vars.revision:
        # В форме редактирования книги задана ревизия элемента книги
        # Требуется показать заданную ревизию элемента книги
        #--------------------------------------------------------------------
        # Достаём из исторической таблицы одну строку с заданной ревизией
        section = db(db.section_history.id == request.vars.revision) \
            .select().first() or error()
        #--------------------------------------------------------------------
        # В поле section таблицы section_history хранится ссылка -  
        # идентификатор строки из таблицы section
        section.id = section.section
        back = forward = None
        pages = []
    else:
        # В форме редактирования книги не задана ревизия элемента книги
        # Требуется показать текущую ревизию элемента книги
        #--------------------------------------------------------------------
        try:
            # Текущий номер главы
            i = int(request.args(0) or 0)
            #----------------------------------------------------------------
            # Текущий номер раздела
            j = int(request.args(1) or 0)
            #----------------------------------------------------------------
            # Строка из таблицы section с заданной главой - i и разделом  - j
            section = db(db.section.chapter_number == i) \
                (db.section.section_number == j).select().first()
        except ValueError:
            error()
        else:
            if not section:
                error()
            #endif
        #endtry
        #--------------------------------------------------------------------
        if len(section.content) < 100: 
            redirect(URL(r=request, args=(i, j + 1)))
        #endif
        #--------------------------------------------------------------------
        i0 = i
        j0 = j
        j0 -= 1
        #--------------------------------------------------------------------
        if j0 < 0: 
            i0 += 1
            j0 = 0
        #endif
        #--------------------------------------------------------------------
        if i0 >= 0:
            back = URL(r=request, args = (i0, j0))
        else:
            back = None
        #endif
        #--------------------------------------------------------------------
        i1 = i
        j1 = j +1
        #--------------------------------------------------------------------
        if not db(db.section.chapter_number == i1) \
           (db.section.section_number == j1).count():
            # Раздел j1 главы i1 - пустой! 
            #----------------------------------------------------------------
            i1 = i + 1
            j1 = 0
        #endif    
        #--------------------------------------------------------------------
        if db(db.section.chapter_number == i1) \
           (db.section.section_number == j1).count(): 
            # Раздел j1 главы i1 - не пустой! 
            #----------------------------------------------------------------
            forward = URL(r = request, args = (i1, j1))
        else:
            # Раздел j1 главы i1 - пустой! 
            forward = None
        #endif    
        #--------------------------------------------------------------------
        pages = db(db.wikipage.active == True) \
            (db.wikipage.section == section.id) \
            .select(db.wikipage.slug, db.wikipage.title)
    #endif    
    #------------------------------------------------------------------------
    content = wiki(section.content)
    #------------------------------------------------------------------------
    return dict(
        section = section, 
        content = content,
        back = back,
        forward = forward,
        pages = pages
    )
#enddef
#============================================================================
def get_docstring(command):
    try:
        lines = eval('%s.__doc__' % command).split('\n')
    except:
        return None
    #endtry
    #------------------------------------------------------------------------
    pad = min([len(x) - len(x.lstrip()) for x in lines if x.strip()])
    #------------------------------------------------------------------------
    return '\n'.join([line[pad:] for line in lines])
#enddef
#============================================================================
@cache(request.env.path_info, 3600)
def docstring():
    if len(request.args) > 2:
        error()
    #endif
    #------------------------------------------------------------------------
    command = '.'.join(request.args)
    #------------------------------------------------------------------------
    if request.vars.revision:
        docstring = db(db.docstring_history.id == request.vars.revision) \
            .select().first() or error()
        #--------------------------------------------------------------------
        id = docstring.id = docstring.docstring
        content = wiki(docstring.content)
    else:
        import cStringIO, sys
        #--------------------------------------------------------------------
        pydoc = local_import('pydoc')
        #--------------------------------------------------------------------
        _s = cStringIO.StringIO()
        '''
        if len(request.args) != 1: 
            error()
        #endif    
        '''
        #--------------------------------------------------------------------
        docstring= db(db.docstring.title == command).select().first()
        #--------------------------------------------------------------------
        if not ALWAYS_RELOAD_DOCSTRING and docstring:
            id = docstring.id
            content = wiki(docstring.content)
        else:
            db(db.docstring.title == command).delete()
            docstring = get_docstring(command)    
            if docstring:
                id = db.docstring.insert(
                    title=command,
                    content=docstring
                )
            else: 
                id, docstring = 0, "No Information"
            #endif
            #----------------------------------------------------------------
            content = wiki(docstring)
    #endif
    #------------------------------------------------------------------------
    attributes = {}
    #------------------------------------------------------------------------
    try:
        for key in eval('dir(%s)' % command):
            if key[:2] != '__':
                attributes[key]=get_docstring('%s.%s' % (command,key)) or ''
            #endif   
        #endfor    
    except:
        pass
    #endtry
    #------------------------------------------------------------------------
    return dict(
        id=id,
        content=content,
        command=command,
        attributes=attributes
    )
#enddef
#============================================================================
def get_wiki():    
    if str(request.args(0)).isdigit():
        page = db(db.wikipage.id == request.args(0)).select().first()
    else:
        page = db(db.wikipage.slug == request.args(0)).select().first()
    #endif
    #------------------------------------------------------------------------
    if not page: 
        error()
    #endif    
    #------------------------------------------------------------------------
    return page
#enddef
#============================================================================
@cache(request.env.path_info, 3600)
def wikipages():
    pages = db(db.wikipage.id > 0).select(
        db.wikipage.title,
        db.wikipage.slug,
        db.wikipage.active,
        orderby=db.wikipage.title
    )
    return dict(pages = pages)
#enddef
#============================================================================
#@cache(request.env.path_info, 3600)
def wikipage():
    if request.vars.revision:
        page = db(db.wikipage_history.id == request.vars.revision) \
            .select().first() or error()
        page.id = page.wikipage
    else: 
        page = get_wiki()
    #endif
    #------------------------------------------------------------------------
    section = db.section[page.section]
    content = wiki(page.content)
    #------------------------------------------------------------------------
    return dict(page=page, content=content, section=section)
#enddef
#============================================================================
@auth.requires_login()
def edit_section():
    if not auth.user.editor: 
        redirect(url('requires_editor'))
    #endif
    #------------------------------------------------------------------------
    section=db(db.section.chapter_number==request.args(0)) \
        (db.section.section_number==request.args(1)) \
        .select().first() or error()
    #------------------------------------------------------------------------
    form = crud.update(
        db.section,
        section,
        onvalidation = lambda form: differ(db.section, form),
        onaccept = lambda form: clone(db.section, form.record),
        next = url('section', request.args)
    )
    #------------------------------------------------------------------------
    form.element(_name='content')['_cols'] = 75
    form.element(_name='content')['_rows'] = 40
    #------------------------------------------------------------------------
    return dict(form=form)
#enddef
#============================================================================
@auth.requires_login()
def edit_docstring():
    if not auth.user.editor: 
        redirect(url('requires_editor'))
    #endif
    #------------------------------------------------------------------------
    docstring = db(db.docstring.title == '.'.join(request.args or [])) \
        .select().first() or error()
    #------------------------------------------------------------------------
    form = crud.update(
        db.docstring,
        docstring,
        onvalidation = lambda form: differ(db.docstring, form),
        onaccept = lambda form: clone(db.docstring, form.record),
        next = url('docstring', request.args)
    )
    #------------------------------------------------------------------------
    form.element(_name='content')['_cols'] = 75
    form.element(_name='content')['_rows'] = 40
    #------------------------------------------------------------------------
    return dict(form=form)
#enddef
#============================================================================
@auth.requires_login()
def create_wikipage():
    db.wikipage.section.default = request.args(0)
    #------------------------------------------------------------------------
    form = crud.create(
        db.wikipage,
        onvalidation = lambda form: form.vars.update(
            dict(slug = IS_SLUG.urlify(form.vars.title))
            ),
        next='wikipage/[id]'
    )
    #------------------------------------------------------------------------
    form.element(_name = 'content')['_cols'] = 75
    form.element(_name = 'content')['_rows'] = 40
    #------------------------------------------------------------------------
    return dict(form = form)
#enddef
#============================================================================
@auth.requires_login()
def edit_wikipage():
    form = crud.update(
        db.wikipage,
        get_wiki(),
        onvalidation = lambda form: differ(db.wikipage, form),
        onaccept = lambda form: clone(db.wikipage, form.record),
        next='wikipage/[id]'
    )
    #------------------------------------------------------------------------
    form.element(_name = 'content')['_cols'] = 75
    form.element(_name = 'content')['_rows'] = 40
    #------------------------------------------------------------------------
    return dict(form=form)
#enddef
#============================================================================
@auth.requires_login()
def log():    
    if not auth.user.editor:
        redirect(url('requires_editor'))
    #endif
    #------------------------------------------------------------------------
    tablename = request.args(0)
    #------------------------------------------------------------------------
    if not tablename + '_history' in db.tables: 
        error()
    #endif
    #------------------------------------------------------------------------
    id = request.args(1) 
    table = db[tablename]
    record = table[id] or error()
    table_history = db[tablename + '_history']
    #------------------------------------------------------------------------
    if 'restore' in request.vars:
        orecord = db(table_history.id == request.vars.restore) \
            (table_history[tablename]==id).select().first()
        #--------------------------------------------------------------------
        if orecord:
            clone(table,record)
            orecord.created_on = request.now
            orecord.created_by = auth.user.id
            orecord.active = True
            del orecord['id']
            del orecord[tablename]
            record.update_record(**dict(orecord))
            redirect(url('log',request.args))
        #endif    
        #--------------------------------------------------------------------
    if request.post_vars:
        for key in request.post_vars:
            if key[:1] == 'h':
                db(table_history.id == key[1:]) \
                    (table_history[tablename]==id).delete()            
            #endif
        #endfor
    #endif    
    #------------------------------------------------------------------------
    history = db(table_history[tablename] == id).select(
        table_history.id,
        table_history.title,
        table_history.changelog,
        table_history.active,
        table_history.diff,
        table_history.created_by,
        table_history.created_on,
        orderby=~table_history.created_on
    )
    #------------------------------------------------------------------------
    return dict(tablename=tablename,record=record,history=history)
#enddef
#============================================================================
@auth.requires_login()
def documents():
    if auth.user.editor and request.vars.delete:
        del db.document[request.vars.delete]
        redirect(url())
    form=crud.create(db.document,request.args(0))
    documents = db(db.document.id>0).select(orderby=db.document.title)
    return dict(form=form,documents=documents)
#enddef
#============================================================================
def api():
    return dict()
#enddef
#============================================================================
def requires_editor():
    return dict()
#enddef
#============================================================================
def user():
    """
    Эта функция включает экспонирование следующих функций:
    http://..../[app]/default/user/login 
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password

    Использование декораторов:
    @auth.requires_login()
    @auth.requires_membership('group name')
    @auth.requires_permission('read','table name',record_id)
    над функциями, которые нуждаются в управлении доступом
    """
    return dict(form=auth())
#enddef
#============================================================================
def download():
    """
    Эта функция позволяет выгружать (downloading) с сервера ранее 
    загруженные (uploaded) на него файлы, включая экспонирование
    следующих функций
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)
#enddef
#============================================================================
def call():
    """
    Эта функция включает экспонирование следующих функций-сервисов:
    http://..../[app]/default/call/jsonrpc
    Используйте декораторы подобные  @services.jsonrpc для экспонируемых
    функций.
    Поддерживаются сервисы: xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    session.forget()
    return service()
#enddef
#############################################################################
