#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#############################################################################
#                                w2pbookru
#############################################################################
__revision__ = "$Id$"
#############################################################################
# make sure administrator is on localhost
import os 
import socket
import datetime
import copy
import gluon.contenttype
import gluon.fileutils
#############################################################################
# critical --- make a copy of the environment
global_env = copy.copy(globals())
global_env['datetime'] = datetime
#----------------------------------------------------------------------------
http_host = request.env.http_host.split(':')[0]
remote_addr = request.env.remote_addr
#----------------------------------------------------------------------------
try:
    hosts = (
        http_host, socket.gethostname(),
        socket.gethostbyname(http_host),
        '::1','127.0.0.1',
        '::ffff:127.0.0.1'
    )   
except:
    hosts = (http_host, )
#endtry
#----------------------------------------------------------------------------
if request.env.http_x_forwarded_for or \
   request.env.wsgi_url_scheme in ['https', 'HTTPS']:
    session.secure()
elif remote_addr not in hosts:
    raise HTTP(200, T('appadmin is disabled because insecure channel'))
#endif
#----------------------------------------------------------------------------
if not gluon.fileutils.check_credentials(request):
    redirect(URL(a='admin', c='default', f='index'))
#endif
#----------------------------------------------------------------------------
ignore_rw = True
#----------------------------------------------------------------------------
# Представление "по умолчанию"
response.view = 'appadmin.html'
#----------------------------------------------------------------------------
# Главное меню
response.menu = [
    [T('design'), False, URL(
        'admin', 'default', 'design',
        args=[request.application])], 
    [T('db'), False, URL(r=request, f='index')], 
    [T('state'), False, URL(r=request, f='state')], 
    [T('cache'), False, URL(r=request, f='ccache')]
]
#============================================================================
def get_databases(request):
    dbs = {}
    #------------------------------------------------------------------------
    for (key, value) in global_env.items():
        cond = False
        #--------------------------------------------------------------------
        try:
            cond = isinstance(value, GQLDB)
        except:
            cond = isinstance(value, SQLDB)
        #endtry
        #--------------------------------------------------------------------
        if cond:
            dbs[key] = value
        #endif
    #endfor
    #------------------------------------------------------------------------
    return dbs
#enddef
#============================================================================
databases = get_databases(None)
#============================================================================
def eval_in_global_env(text):
    exec ('_ret=%s' % text, {}, global_env)
    return global_env['_ret']
#enddef
#============================================================================
def get_database(request):
    if request.args and request.args[0] in databases:
        return eval_in_global_env(request.args[0])
    else:
        session.flash = T('invalid request')
        redirect(URL(r=request, f='index'))
    #endif
#enddef
#============================================================================
def get_table(request):
    db = get_database(request)
    if len(request.args) > 1 and request.args[1] in db.tables:
        return (db, request.args[1])
    else:
        session.flash = T('invalid request')
        redirect(URL(r=request, f='index'))
    #endif
#enddef
#============================================================================
def get_query(request):
    try:
        return eval_in_global_env(request.vars.query)
    except Exception:
        return None
    #endtry
#enddef
#============================================================================
def query_by_table_type(tablename,db,request=request):
    keyed = hasattr(db[tablename],'_primarykey')
    #------------------------------------------------------------------------
    if keyed:
        firstkey = db[tablename][db[tablename]._primarykey[0]]
        cond = '>0'
        if firstkey.type in ['string', 'text']:
            cond = ' != "" '
        qry = '%s.%s.%s%s' % (
            request.args[0], request.args[1], firstkey.name, cond
        )
    else:
        qry = '%s.%s.id > 0' % tuple(request.args[:2])
    #endif
    #------------------------------------------------------------------------
    return qry
#enddef
#============================================================================
def index():
    """List all databases and tables
    """
    #------------------------------------------------------------------------
    return dict(databases = databases)
#enddef
#============================================================================
def insert():
    """Insert a new record
    """
    #------------------------------------------------------------------------
    (db, table) = get_table(request)
    form = SQLFORM(db[table], ignore_rw=ignore_rw)
    #------------------------------------------------------------------------
    if form.accepts(request.vars, session):
        response.flash = T('new record inserted')
    #endif    
    #------------------------------------------------------------------------
    return dict(form=form,table=db[table])
#enddef
#============================================================================
def download():
    import os
    db = get_database(request)
    return response.download(request,db)
#enddef
#============================================================================
def csv():
    """Экспорт данных из таблицы БД в CSV-файл
    """
    #------------------------------------------------------------------------
    import gluon.contenttype
    #------------------------------------------------------------------------
    response.headers['Content-Type'] = gluon.contenttype.contenttype('.csv')
    #------------------------------------------------------------------------
    db = get_database(request)
    query = get_query(request)
    #------------------------------------------------------------------------
    if not query:
        return None
    #endif
    #------------------------------------------------------------------------
    response.headers['Content-disposition'] = \
        'attachment; filename=%s_%s.csv' % tuple(request.vars.query.split('.')[:2])
    #------------------------------------------------------------------------
    return str(db(query).select())
#enddef
#============================================================================
def import_csv(table, file):
    """Импорт данных из CSV-файла в таблицу БД
    """
    table.import_from_csv_file(file)
#enddef
#============================================================================
def select():
    import re
    db = get_database(request)
    dbname = request.args[0]
    regex = re.compile('(?P<table>\w+)\.(?P<field>\w+)=(?P<value>\d+)')
    #------------------------------------------------------------------------
    if len(request.args)>1 and hasattr(db[request.args[1]],'_primarykey'):
        regex = re.compile('(?P<table>\w+)\.(?P<field>\w+)=(?P<value>.+)')
    #endif
    #------------------------------------------------------------------------
    if request.vars.query:
        match = regex.match(request.vars.query)
        if match:
            request.vars.query = '%s.%s.%s==%s' % (
                request.args[0],
                match.group('table'), match.group('field'),
                match.group('value')
            )
        #endif
    else:
        request.vars.query = session.last_query
    #endif
    #------------------------------------------------------------------------
    query = get_query(request)
    #------------------------------------------------------------------------
    if request.vars.start:
        start = int(request.vars.start)
    else:
        start = 0
    #endif
    #------------------------------------------------------------------------
    nrows = 0
    stop = start + 100
    table = None
    rows = []
    orderby = request.vars.orderby
    #------------------------------------------------------------------------
    if orderby:
        orderby = dbname + '.' + orderby
        if orderby == session.last_orderby:
            if orderby[0] == '~':
                orderby = orderby[1:]
            else:
                orderby = '~' + orderby
            #endif
        #endif
    #endif
    #------------------------------------------------------------------------
    session.last_orderby = orderby
    session.last_query = request.vars.query
    #------------------------------------------------------------------------
    form = FORM(TABLE(
        TR(
            T('Query:'), 
            '', 
            INPUT(
                _style='width:400px',
                _name='query', 
                _value=request.vars.query or '',
                requires=IS_NOT_EMPTY(error_message=T("Cannot be empty")))
            ), 
        TR(
            T('Update:'),
            INPUT(
                _name='update_check', 
                _type='checkbox',
                value=False), 
            INPUT(
                _style='width:400px',
                _name='update_fields', 
                _value=request.vars.update_fields  or '')
            ),
        TR(
            T('Delete:'), 
            INPUT(
                _name='delete_check',
                _class='delete',
                _type='checkbox', 
                value=False), 
            ''
            ), 
        TR(
            '', 
            '', 
            INPUT(
                _type='submit', 
                _value='submit'))
        ), 
                _action=URL(r=request,args=request.args)
    )
    #------------------------------------------------------------------------
    if request.vars.csvfile != None:
        try:
            import_csv(
                db[request.vars.table],
                request.vars.csvfile.file
            )
            #----------------------------------------------------------------
            response.flash = T('data uploaded')
        except:
            response.flash = T('unable to parse csv file')
        #endtry
    #endif  
    #------------------------------------------------------------------------
    if form.accepts(request.vars, formname = None):
        # regex = re.compile(request.args[0] + '\.(?P<table>\w+)\.id\>0')
        regex = re.compile(request.args[0] + '\.(?P<table>\w+)\..+')
        #--------------------------------------------------------------------
        match = regex.match(form.vars.query.strip())
        #--------------------------------------------------------------------
        if match:
            table = match.group('table')
        #endif
        #--------------------------------------------------------------------
        try:
            nrows = db(query).count()
            #----------------------------------------------------------------
            if form.vars.update_check and form.vars.update_fields:
                db(query).update(
                    **eval_in_global_env(
                        'dict(%s)' % form.vars.update_fields
                    )
                )
                #------------------------------------------------------------
                response.flash = T('%s rows updated', nrows)
            elif form.vars.delete_check:
                db(query).delete()
                response.flash = T('%s rows deleted', nrows)
            #endif
            #----------------------------------------------------------------
            nrows = db(query).count()
            #----------------------------------------------------------------
            if orderby:
                rows = db(query).select(
                    limitby=(start, stop),
                    orderby=eval_in_global_env(orderby)
                )
            else:
                rows = db(query).select(limitby = (start, stop))
            #endif
        except Exception, e:
            (rows, nrows) = ([], 0)
            response.flash = DIV(T('Invalid Query'),PRE(str(e)))
        #endtry
    #endif
    #------------------------------------------------------------------------
    return dict(
        form=form,
        table=table,
        start=start,
        stop=stop,
        nrows=nrows,
        rows=rows,
        query=request.vars.query,
    )
#enddef
#============================================================================
def update():
    """Edit delete one record
    """
    #------------------------------------------------------------------------
    (db, table) = get_table(request)
    keyed = hasattr(db[table],'_primarykey')
    norec = True
    #------------------------------------------------------------------------
    if keyed:
        key = [f for f in request.vars if f in db[table]._primarykey]
        if key:
            try:
                records = db(db[table][key[0]] == request.vars[key[0]]).select()
                if records:
                    record = records[0]
                    norec = False
                #endif
            except:
                pass
            #endtry
        else:
            qry = query_by_table_type(table, db)
        #endif
    else:
        try:
            id = int(request.args[2])
            record = db(db[table].id == id).select()[0]
            norec = False
        except:
            qry = query_by_table_type(table, db)
        #endtry
    #endif
    #------------------------------------------------------------------------
    if norec:
        session.flash = T('record does not exist')
        redirect(URL(
            r=request, 
            f='select', 
            args=request.args[:1],
            vars=dict(query=qry)
        ))
    #endif
    #------------------------------------------------------------------------
    if keyed:
        for k in db[table]._primarykey: 
            db[table][k].writable = False
        #endfor    
    #endif
    #------------------------------------------------------------------------
    form = SQLFORM(
        db[table], 
        record, 
        deletable=True, 
        delete_label=T('Check to delete'), 
        ignore_rw=ignore_rw and not keyed,
        linkto=URL(
            r=request, 
            f='select',
            args=request.args[:1]
            ), 
        upload=URL(
            r=request,
            f='download', 
            args=request.args[:1],
        )
    )
    #------------------------------------------------------------------------
    if form.accepts(request.vars, session):
        session.flash = T('done!')
        qry = query_by_table_type(table, db)
        #--------------------------------------------------------------------
        redirect(URL(
            r=request, 
            f='select', 
            args=request.args[:1],
            vars=dict(query=qry)
        ))
    #endif    
    #------------------------------------------------------------------------
    return dict(form=form,table=db[table])
#enddef
#============================================================================
def state():
    """Get global variables
    """
    return dict()
#enddef
#============================================================================
def ccache():
    form = FORM(
        P(TAG.BUTTON(
            "Clear CACHE?", _type="submit", _name="yes", _value="yes"
            )),
        P(TAG.BUTTON(
            "Clear RAM", _type="submit", _name="ram", _value="ram"
            )),
        P(TAG.BUTTON(
            "Clear DISK", _type="submit", _name="disk", _value="disk"
            )),
    )
    #------------------------------------------------------------------------
    if form.accepts(request.vars, session):
        clear_ram = False
        clear_disk = False
        session.flash = ""
        #--------------------------------------------------------------------
        if request.vars.yes:
            clear_ram = clear_disk = True
        #endif    
        #--------------------------------------------------------------------
        if request.vars.ram:
            clear_ram = True
        #endif    
        #--------------------------------------------------------------------
        if request.vars.disk:
            clear_disk = True
        #endif    
        #--------------------------------------------------------------------
        if clear_ram:
            cache.ram.clear()
            session.flash += "Ram Cleared "
        #endif    
        #--------------------------------------------------------------------
        if clear_disk:
            cache.disk.clear()
            session.flash += "Disk Cleared"
        #endif    
        #--------------------------------------------------------------------
        redirect(URL(r=request))
    #endif    
    #------------------------------------------------------------------------
    try:
        from guppy import hpy; hp=hpy()
    except:
        hp = False
    #endtry
    #------------------------------------------------------------------------
    import shelve, os, copy, time, math
    from gluon import portalocker
    #------------------------------------------------------------------------
    ram = {
        'bytes': 0,
        'objects': 0,
        'hits': 0,
        'misses': 0,
        'ratio': 0,
        'oldest': time.time()
    }
    #------------------------------------------------------------------------
    disk = copy.copy(ram)
    total = copy.copy(ram)
    #------------------------------------------------------------------------
    for key, value in cache.ram.storage.items():
        if isinstance(value, dict):
            ram['hits'] = value['hit_total'] - value['misses']
            ram['misses'] = value['misses']
            #----------------------------------------------------------------
            try:
                ram['ratio'] = ram['hits'] * 100 / value['hit_total']
            except:
                ram['ratio'] = 0
            #endtry
        else:
            if hp:
                ram['bytes'] += hp.iso(value[1]).size
                ram['objects'] += hp.iso(value[1]).count
                #------------------------------------------------------------
                if value[0] < ram['oldest']:
                    ram['oldest'] = value[0]
                #endif
            #endif
        #endif
    #endfor    
    #------------------------------------------------------------------------
    locker = open(os.path.join(request.folder, 'cache/cache.lock'), 'a')
    portalocker.lock(locker, portalocker.LOCK_EX)
    #------------------------------------------------------------------------
    disk_storage = shelve.open(
        os.path.join(request.folder, 'cache/cache.shelve')
    )
    #------------------------------------------------------------------------
    for key, value in disk_storage.items():
        if isinstance(value, dict):
            disk['hits'] = value['hit_total'] - value['misses']
            disk['misses'] = value['misses']
            try:
                disk['ratio'] = disk['hits'] * 100 / value['hit_total']
            except:
                disk['ratio'] = 0
            #endtry    
        else:
            if hp:
                disk['bytes'] += hp.iso(value[1]).size
                disk['objects'] += hp.iso(value[1]).count
                #------------------------------------------------------------
                if value[0] < disk['oldest']:
                    disk['oldest'] = value[0]
                #endif    
            #endif
        #endif
    #endfor
    #------------------------------------------------------------------------
    portalocker.unlock(locker)
    locker.close()
    disk_storage.close()        
    #------------------------------------------------------------------------
    total['bytes'] = ram['bytes'] + disk['bytes']
    total['objects'] = ram['objects'] + disk['objects']
    total['hits'] = ram['hits'] + disk['hits']
    total['misses'] = ram['misses'] + disk['misses']
    total['ratio'] = (ram['ratio'] + disk['ratio']) / 2
    #------------------------------------------------------------------------
    if disk['oldest'] < ram['oldest']:
        total['oldest'] = disk['oldest']
    else:
        total['oldest'] = ram['oldest']
    #endif    
    #========================================================================
    def GetInHMS(seconds):
        hours = math.floor(seconds / 3600)
        seconds -= hours * 3600
        minutes = math.floor(seconds / 60)
        seconds -= minutes * 60
        seconds = math.floor(seconds)
        #--------------------------------------------------------------------
        return (hours, minutes, seconds)
    #enddef
    #========================================================================
    ram['oldest'] = GetInHMS(time.time() - ram['oldest'])
    disk['oldest'] = GetInHMS(time.time() - disk['oldest'])
    total['oldest'] = GetInHMS(time.time() - total['oldest'])
    #------------------------------------------------------------------------
    return dict(
        form=form, 
        total=total,
        ram=ram, 
        disk=disk
    )
#enddef
#############################################################################
