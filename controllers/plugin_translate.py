#!/usr/bin/env python
# -*- coding: utf-8 -*- 
#############################################################################
#                                w2pbookru
#############################################################################
__revision__ = "$Id$"
#############################################################################
def translate():
    return """jQuery(document).ready(function() {
        jQuery('p, ul').translate('%s');
    });""" % request.args(0).split('.')[0]
#############################################################################

